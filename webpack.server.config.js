const path = require('path');

module.exports = {
  mode: 'none',
  entry: {
    server: './server.js'
  },
  target: 'node',
  resolve: { extensions: ['.js'] },
  optimization: {
    minimize: false
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  }
};
