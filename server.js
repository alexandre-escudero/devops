const express = require('express');
const { join } = require('path');
const { inspect } = require('util');

const app = express();
const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist/devops');

app.get('/invalid', (req, res) => res.sendStatus(500));
const array = [];
app.get('/memory-leak', (req, res) => {
  array.push(inspect(req), inspect(res));
  res.sendStatus(201);
});
app.use(express.static(DIST_FOLDER));
app.listen(PORT);
